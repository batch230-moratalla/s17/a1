console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printWelcomeMessage(){
		let fullName = prompt("What is your name?:");
		let age = prompt("How old are you?:");
		let address = prompt("Where do you live?:");

		// `${variableName}`
		console.log(`Hello, ${fullName}`);
		console.log(`You are ${age} years old.`);
		console.log(`You live in ${address}`);
	}
	printWelcomeMessage();

	function welcomeAlert(){
		alert("Thank you for your input");
	}
	welcomeAlert();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favMusicArtist(){
		console.log("1. The Beatles");
		console.log("2. Metallica");
		console.log("3. The Eagles");
		console.log("4. L`arc`en`Ciel");
		console.log("5. Eraserheads");
	}

	favMusicArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function FavoriteMovies(){
		firstMovie = "1. The God Father";
		ratingOne = "Rotten Tomatoes Rating: 97%";
		secondMovie = "2. The God Father, Part II";
		ratingTwo = "Rotten Tomatoes Rating: 96%";
		thirdMovie = "3. Shawshank Redemption";
		ratingThree = "Rotten Tomatoes Rating: 91%";
		fourthMovie = "4. To Kill Mocking Bird";
		ratingFour = "Rotten Tomatoes Rating: 93%";
		fifthMovie = "5. Psycho";
		ratingFive = "Rotten Tomatoes Rating: 96%";
		
		console.log(firstMovie);
		console.log(ratingOne);
		console.log(secondMovie);
		console.log(ratingTwo);
		console.log(thirdMovie);
		console.log(ratingThree);
		console.log(fourthMovie);
		console.log(ratingFour);
		console.log(fifthMovie);
		console.log(ratingFive);
	}

	FavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
